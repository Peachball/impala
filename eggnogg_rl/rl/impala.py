"""
    Handles synchronous aspect of impala:
    https://arxiv.org/pdf/1802.01561.pdf
"""
import argparse
import os
from collections import namedtuple
from typing import Any, Dict, Tuple, List, Type

import cv2
import gym
import numpy as np
import ray
import tensorflow as tf

from . import models

Rollout = namedtuple('Rollout', ['state', 'action', 'reward', 'prob', 'model_state'])


def create_model(env: gym.core.Env, mdl: models.Model):
    """
        Creates relevant tensorflow model that is parameterized for the
        environment
    """
    return mdl([None] + list(env.observation_space.shape),
               [env.action_space.n])


@ray.remote(num_cpus=1)
class EnvRunner():
    """
        Runs gym environments using a specified model
    """

    def __init__(self, name, model: models.Model, config: Dict = {}) -> None:
        os.environ['CUDA_VISIBLE_DEVICES'] = ','.join(ray.get_gpu_ids())
        self.env = gym.make(name)
        with tf.device('/cpu:0'):
            self.model = create_model(self.env, model)
        self.sess = tf.Session()
        self.sess.run(tf.variables_initializer(self.model.variables))

    def get_run(self, params) -> Any:
        """
            Runs environment simulation with model with said parameters
        """
        self.model.set_params(params, self.sess)
        obs = self.env.reset()
        done = False
        rollout: List[Rollout] = []
        while not done:
            probs, act, state = self.model.act(obs, self.sess)
            prev_obs = obs
            obs, rew, done, _ = self.env.step(act)
            rollout += [Rollout(prev_obs, act, rew, probs, state)]
        # convert rollout to 4 numpy arrays
        obs, act, rew, probs, state = list(zip(*rollout))
        print(np.array(state).shape)
        return (np.array(obs), np.array(act), np.array(rew), np.array(probs),
                np.array(state))


@ray.remote(num_gpus=1)
class Trainer():
    """
        Actor that trains model with V-trace using gpu
    """

    def __init__(self, model: Type[models.Model]) -> None:
        os.environ['CUDA_VISIBLE_DEVICES'] = ','.join(map(str,
                                                          ray.get_gpu_ids()))
        self.model = model()
        self.value_target = tf.placeholder(tf.float32, [None, None])
        self.advantage = tf.placeholder(tf.float32, [None, None])
        self.actions = tf.placeholder(tf.int32, [None, None])

        self.loss = self.calculate_loss(self.model.action, self.model.value)
        self.sess = tf.Session()
        self.sess.run(tf.variables_initializer(self.model.variables))

    def calculate_loss(self, action_logits: tf.Tensor, value: tf.Tensor) -> tf.Tensor:
        """
            Calculates losses that trainer will use
        """
        seq_mask = tf.sequence_mask(self.model.seq_lengths, dtype=tf.float32)
        vloss = tf.squared_difference(value, self.value_target) / 2
        vloss = tf.reduce_sum(vloss * seq_mask)

        act = tf.nn.log_softmax(action_logits)
        act_mask = tf.one_hot(self.actions, self.model.out_dim[0], axis=-1)
        log_act = tf.reduce_sum(act * act_mask, axis=-1)
        ploss = log_act * seq_mask * self.advantage

        return ploss + 0.25 * vloss

    def get_data(self, rollout: Rollout):
        """
            Parses rollout and calculates the advantage/truncated importance
            sampling of each observation, and the other data needed for the
            training
        """
        data = []
        all_obs, _, all_rewards, all_probs, _ = zip(*rollout)
        values, probs = self.calculate_vp(all_obs)

        default_rho = 1
        default_c = 1
        rho = np.min(default_rho, probs / all_probs)
        c = np.min(default_c, probs / all_probs)
        value_target = self.calculate_value_targets(values, all_rewards, rho, c)
        for obs, _, _, _, _ in rollout:
            img = cv2.resize(obs, dsize=(84, 84), interpolation=cv2.INTER_CUBIC)
            data += [img]

        reward = 0
        lmbda = 0.99
        for i in range(len(rollout) - 1, -1, -1):
            reward = rollout[i][2] + lmbda * reward

    def calculate_value_targets(self, value: np.ndarray, reward: np.ndarray,
                                rho: np.ndarray, c: np.ndarray):
        """
            Calculates v_s
        """
        c_prods = np.zeros_like(c)
        for i in range(c.shape[0]):
            pass

    def calculate_vp(self, obs: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """
            Calculates the value and probabilities based off observations
        """
        return self.sess.run([self.model.value, self.model.action],
                             feed_dict={
                                 self.model.inp: obs[None],
                                 self.model.seq_lengths: [obs.shape[0]]
                             })

    def get_parameters(self):
        return [self.sess.run(v) for v in self.model.variables]

    def train(self, rollouts: List[Rollout]):
        pass


def get_args():
    """
        Gets arguments passed to function
    """
    parser = argparse.ArgumentParser(description='Train a rl agent with impala')

    parser.add_argument('--server', default='192.168.0.6:3333', type=str,
                        help="Redis server address to connect to")
    parser.add_argument('--workers', default=1, type=int,
                        help='Number of workers to simulate environment')
    parser.add_argument('--model', default='lstm', type=str,
                        help='Type of model to train')
    parser.add_argument('--env', default='Pong-v3', type=str,
                        help='Environment to train model in')
    parser.add_argument('--head', default=True, type=bool,
                        help='Whether or not to create own server or not')
    parser.add_argument('--config', default=None, type=str,
                        help='Configuration file')

    return parser.parse_args()


def main() -> None:
    """
    Handles user input
    """
    args = get_args()
    if args.head:
        ray.init(num_gpus=1)
    else:
        ray.init(redis_address=args.server)

    mdl = models.get_model(args.model)
    with tf.variable_scope('global'):
        trainer = Trainer.remote(lambda: create_model(gym.make(args.env), mdl))
    params = trainer.get_parameters.remote()
    workers = []
    for i in range(1):
        with tf.variable_scope('worker{}'.format(i)):
            workers += [EnvRunner.remote(args.env, mdl)]

    env = gym.make(args.env)

    ray.get(workers[0].get_run.remote(params))


if __name__ == '__main__':
    main()
