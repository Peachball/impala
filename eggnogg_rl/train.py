import copy
import faulthandler
import logging
import multiprocessing.connection
import time
from pathlib import Path
from typing import Callable, Tuple, List, Iterator, Union

import gym
import numpy as np
import torch
import torch.multiprocessing as mp
from tensorboardX import SummaryWriter
import pickle

from eggnogg_rl import launcher
from eggnogg_rl.rl import ppo

DEBUG = False


class Rollout:
    def __init__(self):
        self.obs: List[np.ndarray] = []
        self.act: List[Tuple[np.ndarray, np.ndarray]] = []
        self.rew: List[Tuple[float, float]] = []
        self.prob: List[Tuple[np.ndarray, np.ndarray]] = []
        self.pos: int = 0
        self.seen: int = 0
        self.total_time: float = 0

        # True if most recent policy is on the right side (player 2)
        # False if most recent policy is on the left side (player 1)
        # None if unknown (default)
        self.current_policy_side: Union[bool, None] = None

    def __len__(self):
        return len(self.act)


def get_eggnogg_run(env: gym.Env,
                    model: Callable[[np.ndarray, torch.Tensor], Tuple[Tuple[
                        Tuple[np.ndarray, np.ndarray], Tuple[
                            np.ndarray, np.ndarray]], torch.Tensor]],
                    max_steps: int = 1000 if DEBUG else 1000) -> Rollout:
    rollout = Rollout()
    obs = env.reset()
    rollout.obs.append(obs)
    done = False
    state = None
    total_steps = 0
    tic = time.time()
    while total_steps < max_steps and not done:
        (act, p_act), state = model(obs, state)
        rollout.act.append(act)
        rollout.prob.append(p_act)
        obs, rew, done, _ = env.step(act)
        rollout.obs.append(obs)
        rollout.rew.append(rew)
        total_steps += 1
    toc = time.time()

    rollout.total_time = toc - tic

    return rollout


class RolloutWorker(mp.Process):
    def __init__(self,
                 data_queue: mp.Queue,
                 should_stop: multiprocessing.connection.Connection,
                 global_policy: ppo.LSTMPolicy,
                 old_policy_dir: Union[Path, None] = None) -> None:
        super(RolloutWorker, self).__init__()
        self.data_queue = data_queue
        self._should_stop = should_stop
        self.global_policy = global_policy

    def run(self):
        with launcher.temp_vnc_env() as env:
            policy = ppo.LSTMPolicy(env.observation_space,
                                    env.action_space.spaces[0])
            old_policy = ppo.LSTMPolicy(env.observation_space,
                                        env.action_space.spaces[0])
            old_policy.cpu()
            old_policy.eval()
            policy.cpu()
            policy.eval()
            while not self._should_stop.poll():
                try:
                    with torch.no_grad():
                        policy.load_state_dict(self.global_policy.state_dict())
                    left_policy, right_policy = policy, old_policy
                    policy_side = False
                    if np.random.rand() < 0.5:
                        left_policy, right_policy = old_policy, policy
                        policy_side = True

                    rollout = get_eggnogg_run(
                        env, self.get_policy(left_policy, right_policy))

                    rollout.current_policy_side = policy_side
                    self.data_queue.put(rollout)
                    logging.info("Added training run. New size: {}".format(
                        self.data_queue.qsize()))
                    logging.info("[P-{}] Average fps: {} for {} frames".format(
                        mp.current_process().pid,
                        len(rollout) / rollout.total_time, len(rollout)))
                except TypeError:
                    time.sleep(5)

    def get_policy(self, policy_left, policy_right):
        def _act(obs, state):
            with torch.no_grad():
                if state is None:
                    state = [None, None]
                ((left_act, _), lprob), left_state = policy_left.get_action(
                    obs, state[0])
                ((_, right_act), rprob), right_state = policy_right.get_action(
                    obs, state[1])
            action_probs = np.stack((lprob[0], rprob[1]))
            return ((left_act, right_act),
                    action_probs), [left_state, right_state]

        return _act


def preprocess_rollout(rollout: Rollout) -> Rollout:
    """
    Preprocessing steps:
        1. Normalizes pixel values
    """
    all_obs = np.stack(rollout.obs).astype('float32')
    all_obs /= 255.
    rollout.obs = [v[0] for v in np.split(all_obs, all_obs.shape[0], axis=0)]
    return rollout


def _batched_rollout_iter(
        batch_fn: Callable[[], Rollout],
        timesteps: int,
        batch_size: int,
        place_batch: Callable[[Rollout], None] = lambda v: None):
    batch = []
    for i in range(batch_size):
        batch.append(batch_fn())

    while True:
        batched_obs: List[np.ndarray] = []
        mask: List[np.ndarray] = []
        batched_acts: List[np.ndarray] = []
        batched_p_acts: List[np.ndarray] = []
        batched_rewards: List[np.ndarray] = []
        batched_player_ind: List[np.ndarray] = []
        for i in range(len(batch)):
            obs_batch: List[np.ndarray] = []
            mask_batch: List[np.ndarray] = []
            act_batch: List[np.ndarray] = []
            rew_batch: List[np.ndarray] = []
            p_act_batch: List[np.ndarray] = []
            player_batch: List[np.ndarray] = []
            total_steps = 0
            while total_steps < timesteps:
                b = batch[i]
                num_steps = timesteps - total_steps
                all_obs = np.array(
                    b.obs[b.pos:b.pos + num_steps],
                    dtype=np.float32)  # (bs, *img_dim)
                obs_batch.append(all_obs)
                player_batch.append(
                    np.repeat(b.current_policy_side, all_obs.shape[0]))
                acts = np.array(b.act[b.pos:b.pos + all_obs.shape[0] - 1])
                p_acts = np.array(b.prob[b.pos:b.pos + acts.shape[0]])
                rewards = np.array(b.rew[b.pos:b.pos + acts.shape[0]])

                if acts.shape[0] != 0:
                    act_batch.append(acts)
                    p_act_batch.append(p_acts)
                    rew_batch.append(rewards)

                # Add processed actions (pad the end)
                act_batch.append(np.zeros((1, 2, 6)))
                p_act_batch.append(np.zeros((1, 2, 6)))
                rew_batch.append(np.zeros((1, 2)))
                m = np.zeros((all_obs.shape[0], ))
                if b.pos == 0:
                    m[0] = 1
                mask_batch.append(m)

                # Last state used for reward bootstrapping
                total_steps += all_obs.shape[0]
                b.pos += num_steps
                if b.pos >= len(b):
                    place_batch(batch[i])
                    batch[i] = batch_fn()
            batched_acts.append(np.concatenate(act_batch))
            batched_p_acts.append(np.concatenate(p_act_batch))
            batched_obs.append(np.concatenate(obs_batch, axis=0))
            batched_rewards.append(np.concatenate(rew_batch))
            batched_player_ind.append(np.concatenate(player_batch))
            mask.append(np.concatenate(mask_batch, axis=0))
        all_obs = np.stack(batched_obs, axis=1)
        all_mask = np.stack(mask, axis=1)
        all_game_acts = np.stack(batched_acts)
        all_game_p_acts = np.stack(batched_p_acts)
        all_rew = np.stack(batched_rewards, axis=1)
        all_player_ind = np.stack(batched_player_ind, axis=1)

        # adjust actions
        def _reshape_act(acts):
            return np.transpose(
                np.concatenate((acts[:, :, 0, :], acts[:, :, 1, :]), axis=0),
                (1, 0, 2))

        # Reshape and adjust arrays
        all_game_acts = _reshape_act(all_game_acts)
        all_game_p_acts = _reshape_act(all_game_p_acts)
        all_rew = np.concatenate((all_rew[:, :, 0], all_rew[:, :, 1]), axis=1)

        yield (all_obs, all_game_acts, all_game_p_acts, all_mask, all_rew,
               all_player_ind)


def calculate_n_step_advantage_rewards(
        value: torch.Tensor, rewards: torch.Tensor, mask: torch.Tensor,
        discount: float, lam: float) -> Tuple[torch.Tensor, torch.Tensor]:
    """
    Arguments:
        value: result of value function
    """
    value_deltas = discount * value[1:, :, 0] \
                   - value[: -1, :, 0] + rewards
    generalized_adv = np.zeros_like(value[:, :, 0])
    discounted_reward = np.zeros_like(generalized_adv)
    generalized_adv[-1, :] = value_deltas[-1, :]
    discounted_reward[-1, :] = value[-1, :, 0]
    for i in range(generalized_adv.shape[0] - 2, -1, -1):
        r = rewards[i, :]
        m = mask[i + 1, :]
        updated_reward = (r + lam * discounted_reward[i + 1, :])
        discounted_reward[
            i, :] = (1 - m) * updated_reward + m * (value[i, :, 0])
        generalized_adv[i, :] = value_deltas[i, :] + (
            1 - m) * lam * discount * generalized_adv[i + 1, :]
    return discounted_reward, generalized_adv


class TrainSettings:
    """
    Settings object with reasonable defaults on training process
    """

    def __init__(self):
        self.batch_size = 2 if DEBUG else 32
        self.timesteps = 64 if DEBUG else 256
        self.discount = .99
        self.lam = .95
        self.epsilon = .2  # For policy ratio clipping in ppo
        self.logdir = 'logs'
        self.max_seen = 5


class Trainer:
    def __init__(self, data_queue, model, shared_model,
                 settings: TrainSettings) -> None:
        self.data_queue = data_queue
        self.model = model
        self.shared_model = shared_model
        self.settings = settings
        self.sw = SummaryWriter(str(Path(self.settings.logdir)))
        self.step = 0
        self.optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)

    def train(self, steps=10**100):
        bs = self.settings.batch_size
        for _ in range(steps):
            hidden = self.model.init_hidden(bs)
            for obs, acts, p_acts, mask, rew, player in _batched_rollout_iter(
                    self.pull_batch,
                    self.settings.timesteps,
                    bs,
                    place_batch=self.place_batch):
                _get_player = lambda v: v[:, bs:, :] * player[:, :, None] + \
                        (1 - player[:, :, None]) * v[:, :bs, :]
                acts = _get_player(acts)
                p_acts = _get_player(p_acts)
                rew = rew[:, bs:] * player + rew[:, :bs] * (1 - player)
                # Make pytorch not try to propagate gradients from previous
                # updates through the hidden state
                hidden = tuple(h.detach() for h in hidden)
                self.optimizer.zero_grad()

                (all_embeddings, all_acts,
                 all_values) = self.model.batch_predictions(
                     obs, hidden, mask,
                     torch.from_numpy(player.astype('float32')).cuda())

                curiosity_rewards = self.state_model_error(
                    all_acts, all_embeddings)
                all_rewards = torch.cat(
                    [curiosity_rewards[:, :bs], curiosity_rewards[:, bs:]],
                    dim=1).detach().cpu().numpy() * 0 + rew[:-1]
                value_copy = all_values.detach().cpu().numpy()
                discounted_reward, generalized_adv = (
                    calculate_n_step_advantage_rewards(
                        value_copy, all_rewards, mask, self.settings.discount,
                        self.settings.lam))

                self.sw.add_scalar('adv/mean',
                                   generalized_adv.mean().item(), self.step)
                self.sw.add_scalar('adv/std',
                                   generalized_adv.std().item(), self.step)

                inverted_mask = torch.from_numpy(
                    (1 - mask[1:, :]).astype('float32')).cuda()

                value_loss = self.calculate_value_loss(
                    all_values, discounted_reward, inverted_mask)
                self.sw.add_scalar('value_loss', value_loss.item(), self.step)

                # Calculate policy loss
                policy_loss = self.calculate_policy_loss(
                    acts, all_acts, generalized_adv, inverted_mask, p_acts)
                self.sw.add_scalar('policy_loss', policy_loss.item(),
                                   self.step)
                state_loss = (curiosity_rewards *
                              inverted_mask).sum() / inverted_mask.sum()
                self.sw.add_scalar('state_loss', state_loss.item(), self.step)

                loss = .25 * value_loss + 4 * policy_loss + .1 * state_loss
                self.sw.add_scalar('total_loss', loss.item(), self.step)
                loss.backward()
                self.optimizer.step()
                self.shared_model.load_state_dict(self.model.state_dict())
                print("Loss", loss.item())
                self.step += 1

    def calculate_policy_loss(self, acts, all_acts, generalized_adv,
                              inverted_mask, p_acts):
        act_tensor = torch.from_numpy(acts.astype('float32')).cuda()
        _get_prob = lambda a: act_tensor * a + (1 - act_tensor) * (1 - a)
        act_probs = _get_prob(torch.sigmoid(all_acts))
        act_entropy = -(
            (act_probs * torch.log(act_probs) +
             (1 - act_probs) * torch.log(1 - act_probs)).sum(dim=-1)[:-1] *
            inverted_mask).sum() / inverted_mask.sum()
        self.sw.add_scalar('entropy', act_entropy, self.step)
        policy_ratios = (
            _get_prob(torch.sigmoid(all_acts)).prod(dim=-1) / _get_prob(
                torch.from_numpy(
                    p_acts.astype('float32')).cuda()).prod(dim=-1))[:-1, :]
        adv = generalized_adv[:-1, :]
        normalized_adv = torch.from_numpy(
            (adv - adv.mean()) / (adv.std() + 1e-8)).cuda()
        policy_loss = -(torch.min(
            torch.clamp(policy_ratios, 1 - self.settings.epsilon, 1 +
                        self.settings.epsilon) * normalized_adv, policy_ratios
            * normalized_adv) * inverted_mask).sum() / inverted_mask.sum()
        return policy_loss

    def calculate_value_loss(self, all_values, discounted_reward,
                             inverted_mask):
        huber_loss = torch.nn.SmoothL1Loss(reduction='none')
        all_value_loss = huber_loss(
            all_values[:-1, :, 0],
            torch.from_numpy(discounted_reward[:-1, :]).cuda()) * inverted_mask
        value_loss = all_value_loss.sum() / inverted_mask.sum()
        return value_loss

    def state_model_error(self, all_acts, all_embeddings):
        predicted_embeddings = self.model.state_model(
            torch.cat(
                [
                    all_embeddings[:-1].detach().view(
                        -1, self.model.hidden_size), all_acts[:-1].view(
                            -1,
                            all_acts.size()[-1])
                ],
                dim=-1)).view(*all_embeddings[:-1].size()[:-1],
                              all_embeddings.size()[-1])
        curiosity_rewards = (
            all_embeddings.detach()[1:] - predicted_embeddings).norm(dim=-1)
        return curiosity_rewards

    def pull_batch(self):
        rollout = self.data_queue.get()
        if rollout.seen == 0:
            rollout = preprocess_rollout(rollout)
        rollout.pos = 0
        return rollout

    def place_batch(self, batch):
        if batch.seen == 0:
            self.sw.add_scalar('episode_length', len(batch), self.step)
            total_rewards = np.array(batch.rew).sum(axis=0)[int(
                batch.current_policy_side)]
            self.sw.add_scalar('reward/all', total_rewards, self.step)
            if batch.current_policy_side:
                self.sw.add_scalar('reward/left', total_rewards, self.step)
            else:
                self.sw.add_scalar('reward/right', total_rewards, self.step)
        batch.seen += 1
        if batch.seen <= self.settings.max_seen:
            logging.info("Re-added batch (seen {})".format(batch.seen))
            self.data_queue.put(batch)


class RolloutWorkerPool:
    def __init__(self, num_workers, policy):
        self.data_queue = mp.Queue(100)
        self.num_workers = num_workers
        self.pipes = [mp.Pipe() for _ in range(num_workers)]
        self.workers = [
            RolloutWorker(self.data_queue, p[1], policy) for p in self.pipes
        ]

    def start(self):
        for w in self.workers:
            logging.info("Spawning processes")
            w.start()
            time.sleep(3)

    def stop(self):
        for p in self.pipes:
            p[0].send(None)


def get_eggnogg_policy(*args, **kwargs):
    with launcher.temp_vnc_env() as env:
        return ppo.LSTMPolicy(env.observation_space,
                              env.action_space.spaces[0], *args, **kwargs)


def regenerate_test_rollouts(path):
    path = Path(path)
    policy = get_eggnogg_policy()
    workers = RolloutWorkerPool(1, policy)
    workers.start()

    rollouts = []
    for _ in range(10):
        rollouts.append(workers.data_queue.get())

    workers.stop()

    with path.open('wb') as f:
        pickle.dump(rollouts, f)


def get_test_rollouts(path, repeats=10):
    with Path(path).open('rb') as f:
        saved = pickle.load(f)

    queue = mp.Queue(100)
    for _ in range(repeats):
        for r in saved:
            queue.put(copy.deepcopy(r))
    return queue


def main():
    torch.set_num_threads(1)
    try:
        # Spawn worker threads getting data
        workers = 4
        shared_policy = get_eggnogg_policy()
        cuda_policy = copy.deepcopy(shared_policy)
        shared_policy.cpu()
        shared_policy.share_memory()
        cuda_policy.cuda()
        cuda_policy.train()
        shared_policy.load_state_dict(cuda_policy.state_dict())
        workers = RolloutWorkerPool(workers, shared_policy)
        # workers.data_queue = get_test_rollouts('data/rollouts.pkl')
        workers.start()

        trainer = Trainer(workers.data_queue, cuda_policy, shared_policy,
                          TrainSettings())
        trainer.train()

    finally:
        launcher.kill_all_vncservers()
        workers.stop()


if __name__ == '__main__':
    faulthandler.enable()
    logging.basicConfig(level=logging.INFO)
    main()
    # regenerate_test_rollouts('data/rollouts.pkl')
