import time

from eggnogg_rl import launcher


def calculate_fps(sample_frames=40) -> float:
    with launcher.temp_vnc_game() as game:
        start = time.time()
        for i in range(sample_frames):
            frame = game.get_frame()
    total_time = time.time() - start
    return sample_frames / total_time
