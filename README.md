# EGGNOG
An interactive reinforcement learning environment to play eggnogg+.

## Required Libraries


- Linux
   - TigerVNC
   - xdotool
   - twm (probably already installed)
   - One of:
     - maim
     - ImageMagick
     - python-xlib (fastest)
- Windows
  - Not supported yet
