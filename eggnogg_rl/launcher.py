import abc
import enum
import multiprocessing as mp
import os
import stat
import subprocess
import tempfile
import time
from contextlib import contextmanager
from pathlib import Path
from subprocess import Popen
from typing import Iterator, List, Union, Tuple, Dict, Optional, Any

import Xlib
import cv2
import gym
import numpy as np
import pytesseract
from PIL import Image
from Xlib import display, X, XK
from numpy import ndarray

EXECUTABLE_LOCATION = '/home/peachball/D/Linux/Games/eggnoggplus-linux/eggnoggplus'
KEYPRESS_DELAY = 0.015
LOAD_DELAY = .5


class XlibWindow:
    def __init__(self, wid: int, disp: str) -> None:
        self._wid = wid
        self._disp = disp
        self._dsp = display.Display(disp)
        self._resource = self._dsp.create_resource_object('window', wid)
        self._geom = self._resource.get_geometry()

    def get_screen(self):
        screen_bytes = self._resource.get_image(
            0, 0, self._geom.width, self._geom.height, X.ZPixmap, 0xffffffff)
        img = Image.frombytes("RGB", (self._geom.width, self._geom.height),
                              screen_bytes.data, "raw", "BGRX")
        return np.array(img)

    def send_key(self, key, keypress=True):
        evt = self._create_xlib_keyevent(key, keypress)
        self._dsp.send_event(self._resource, evt, propagate=True)
        self._dsp.sync()

    def _create_xlib_keyevent(self, key, keypress=True):
        keysym = XK.string_to_keysym(key)
        keycode = self._dsp.keysym_to_keycode(keysym)
        kwargs = {
            'time': int(time.time()),
            'window': self._resource,
            'root': self._dsp.screen().root,
            'same_screen': 0,
            'child': Xlib.X.NONE,
            'root_x': 0,
            'root_y': 0,
            'event_x': 0,
            'event_y': 0,
            'state': X.ShiftMask,
            'detail': keycode,
        }
        if keypress:
            return Xlib.protocol.event.KeyPress(**kwargs)
        else:
            return Xlib.protocol.event.KeyRelease(**kwargs)


def _cmd_get_screen(command: List[Union[str, None]]) -> ndarray:
    """
        Function that returns the appropriate function to get the screen
    """

    def func(win, disp=':0') -> ndarray:
        command = [str(win) if c is None else c for c in command]
        screen_bytes = _run(command, disp, raw=True)
        nparr = np.fromstring(screen_bytes, np.uint8)
        return cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    return func


def _send_event(window, event, display=':0'):
    return CONTROL_MAP[CONTROL_BACKEND](window, event, _parse_(display))


def _run(args: List[str],
         display: Optional[str] = None,
         raw: bool = False,
         **kwargs) -> str:
    output = subprocess.run(
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        env=_get_env(display),
        **kwargs).stdout
    if raw:
        return output
    else:
        return output.decode('utf-8')


def _make_executable(filename: str) -> None:
    st = os.stat(filename)
    os.chmod(filename, st.st_mode | stat.S_IEXEC)


def create_vnc_server(startup_script: List[str]) -> str:
    _, xstartup_fn = tempfile.mkstemp()
    try:
        with open(xstartup_fn, 'w') as f:
            f.write('\n'.join(startup_script))
        _make_executable(xstartup_fn)
        args = ['vncserver', '-xstartup', xstartup_fn]
        output = _run(args, shell=True)
        result = output.split('\n')
        return result[1].strip().split(' ')[-1]
    finally:
        os.remove(xstartup_fn)


def _default_xstartup() -> List[str]:
    return ['unset SESSION_MANAGER', 'unset DBUS_SESSION_BUS_ADDRESS\n']


def kill_vnc_server(display: str) -> None:
    _run(['vncserver', '-kill', display])


def kill_all_vncservers() -> None:
    out = _run(['vncserver', '-list'])
    lines = out.strip().split('\n')
    if len(lines) < 4:
        return
    for l in lines[3:]:
        kill_vnc_server(l.split('\t')[0])


def _get_env(display: Optional[str]) -> Dict[str, str]:
    new_env = os.environ.copy()
    if display is not None:
        new_env['DISPLAY'] = ':' + display.split(':')[-1]
    return new_env


def get_window_id(pid: Union[int, str], display: Optional[str] = None) -> int:
    window_id = 0
    while window_id == 0:
        res = _run(['xdotool', 'search', '--pid', str(pid)], display)
        if len(res) != 0:
            try:
                window_id = int(res)
            except ValueError:
                pass
    return window_id


def run_eggnogg(display: Optional[str] = None) -> Popen:
    return subprocess.Popen(
        [EXECUTABLE_LOCATION],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.STDOUT,
        cwd=Path(EXECUTABLE_LOCATION).parent,
        env=_get_env(display))


class GameScreen(enum.Enum):
    TITLE = 0
    LEVEL_SELECT = 1
    GAME = 2
    PAUSE = 3


class Eggnogg(abc.ABC):
    def __init__(self):
        self.initialize_game()
        self._xlib = XlibWindow(self.window_id, self.display)

    def send_keyevent(self, k: str, keypress: bool) -> None:
        self._xlib.send_key(k, keypress=keypress)

    def click(self, k, delay=KEYPRESS_DELAY) -> None:
        self.send_keyevent(k, True)
        time.sleep(delay)
        self.send_keyevent(k, False)
        time.sleep(delay)

    def get_frame(self) -> ndarray:
        return self._xlib.get_screen()

    def current_mode(self) -> GameScreen:
        return get_game_mode(self.get_frame())

    def pause_game(self):
        if self.current_mode() == GameScreen.GAME:
            self.click('p')

    def reset_game(self):
        self._reset()
        while self.current_mode() != GameScreen.GAME:
            self._reset()

    def _reset(self):
        transitions = {
            GameScreen.GAME: 'paaaawwwwvvv',
            GameScreen.PAUSE: 'aaaawwwwvvv',
            GameScreen.LEVEL_SELECT: 'v',
            GameScreen.TITLE: 'vv'
        }
        for c in transitions[self.current_mode()]:
            self.click(c, delay=.10)

    @abc.abstractmethod
    def initialize_game(self) -> None:
        raise NotImplementedError()

    @abc.abstractmethod
    def close(self) -> None:
        raise NotImplementedError()

    @property
    @abc.abstractmethod
    def display(self):
        pass

    @property
    @abc.abstractmethod
    def window_id(self):
        pass


class DesktopEggnogg(Eggnogg):
    def initialize_game(self) -> None:
        self.process = run_eggnogg()
        time.sleep(LOAD_DELAY)  # Wait for window to load
        self._window_id = get_window_id(self.process.pid)
        self._display = ':0'

    @property
    def display(self):
        return self._display

    @property
    def window_id(self):
        return self._window_id

    def close(self) -> None:
        self.process.kill()


class VNCEggnogg(Eggnogg):
    def initialize_game(self) -> None:
        self.server = VNCServer(_default_xstartup())
        time.sleep(LOAD_DELAY)
        self._display = ':' + self.server.display_id.split(':')[-1]
        self.process = run_eggnogg(display=self.server.display_id)
        time.sleep(LOAD_DELAY)
        self._window_id = get_window_id(self.process.pid,
                                        self.server.display_id)

    @property
    def display(self):
        return self._display

    @property
    def window_id(self):
        return self._window_id

    def close(self) -> None:
        self.server.close()


class VNCServer():
    def __init__(self, startup_command: List[str]) -> None:
        self.display_id = create_vnc_server(startup_command)

    def close(self) -> None:
        kill_vnc_server(self.display_id)


class VNCEggnoggEnv(gym.Env):
    def __init__(self, img_dim=(128, 128), grayscale=False):
        self._img_dim = img_dim
        self._grayscale = grayscale
        self.game = VNCEggnogg()
        self._prev_frame = None
        self.action_space = gym.spaces.Tuple(
            tuple(gym.spaces.MultiBinary(6) for _ in range(2)))
        self.observation_space = gym.spaces.Box(
            low=0,
            high=255,
            shape=tuple(list(img_dim) + [1 if grayscale else 3]),
            dtype=np.float32)
        self._pool = mp.Pool()

    def step(self, action: Tuple[np.ndarray, np.ndarray]
             ) -> Tuple[np.ndarray, Tuple[float, float], bool, Dict[str, int]]:
        assert self.action_space.contains(action)
        key_mapping = {
            0: ('w', 'Up'),
            1: ('a', 'Left'),
            2: ('s', 'Down'),
            3: ('d', 'Right'),
            4: ('v', 'comma'),
            5: ('b', 'period')
        }
        tic = time.time()
        commands = [(v, action[i == 0][k] == 1)
                    for k, vs in key_mapping.items() for i, v in enumerate(vs)]
        random.shuffle(commands)
        for key, keypress in commands:
            self._send_key(key, keypress=keypress)
        frame = self.game.get_frame()
        obs = self._process_obs(frame)
        rew, done = self._determine_reward(frame)
        return obs, rew, done, {}

    def _get_obs(self):
        img = self.game.get_frame()
        return self._process_obs(img)

    def _process_obs(self, img):
        if self._img_dim is not None:
            img = cv2.resize(img, self._img_dim)
        if self._grayscale:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        return img

    def _send_key(self, v, keypress):
        self.game.send_keyevent(v, keypress=keypress)

    def _determine_reward(self, frame) -> Tuple[Tuple[float, float], bool]:
        """
        Eggnogg reward function

        Determines who is alive/dead based on who has arrow on corner of screen.

        Returns tuple of reward, (left player, right player)
        """
        ARROW_PIXELS = 3000
        left_corner = frame[:200, :200, :]
        right_corner = frame[:200, -200:, :]

        def _check_arrow(f):
            hsv = cv2.cvtColor(f, cv2.COLOR_BGR2HSV)
            return (hsv[:, :, 2] > 250).sum() > ARROW_PIXELS

        rewards = [0., 0.]
        done = False

        # Check if someone died
        left_arrow = _check_arrow(left_corner),
        right_arrow = _check_arrow(right_corner)
        if left_arrow ^ right_arrow:
            winning = 1 if left_arrow else 0
            rewards[winning] += 1
            rewards[~winning] -= 1
            done = True
        return (rewards[0], rewards[1]), done

    def reset(self):
        reset_keylist = [
            'a', 's', 'w', 'd', 'v', 'b', 'comma', 'period', 'Up', 'Left',
            'Right', 'Down'
        ]
        for k in reset_keylist:
            self.game.send_keyevent(k, keypress=False)
            time.sleep(.1)

        self.game.reset_game()
        obs = self._get_obs()
        self._prev_frame = obs
        return obs

    def render(self):
        cv2.imshow('eggnogg', self.game.get_frame())
        cv2.waitKey(1)

    def close(self):
        self.game.close()
        cv2.destroyAllWindows()
        self._pool.close()


@contextmanager
def temp_game() -> Iterator[DesktopEggnogg]:
    g = DesktopEggnogg()
    try:
        yield g
    finally:
        g.close()


@contextmanager
def temp_vnc_game() -> Iterator[VNCEggnogg]:
    game = VNCEggnogg()
    try:
        yield game
    finally:
        game.close()


@contextmanager
def temp_vnc_env(*args, **kwargs) -> Iterator[VNCEggnoggEnv]:
    env = VNCEggnoggEnv(*args, **kwargs)
    try:
        yield env
    finally:
        env.close()


def display_gameview_window(game) -> None:
    try:
        while True:
            frame = game.get_frame()
            cv2.imshow('View:', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    finally:
        cv2.destroyAllWindows()


def get_game_mode(image: np.ndarray) -> GameScreen:
    """
    WARNING: Behavior of this method depends HEAVILY on the behavior of
    pytesseract. Oftentimes the library incorrectly reads text of the screen,
    and that incorrect reading is what is compared.
    """
    _check = lambda crop, text: pytesseract.image_to_string(crop) == text

    # Check title
    if _check(image[70:300, 70:770, :], 'EEEHDEE'):
        return GameScreen.TITLE

    # Check level select
    if _check(image[20:90, 280:670, :], 'HHEHH SELECT'):
        return GameScreen.LEVEL_SELECT

    # Check pause menu
    if _check(image[20:90, 380:580, :], 'FHUEEIJ'):
        return GameScreen.PAUSE

    return GameScreen.GAME
