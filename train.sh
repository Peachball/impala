#!/usr/bin/env bash

kill $(pgrep tensorboard) # Kill ALL existing tensorboard instances (dangerous)
sleep 0.5 # give tensorboard a sec to die
rm -r logs/
sleep 0.5 # give tensorboard a sec to look at new log files
tensorboard --logdir logs &
OMP_NUM_THREADS=1 optirun python eggnogg_rl/train.py
