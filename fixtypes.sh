#!/usr/bin/env bash
args=--include-unparsable-defaults
monkeytype $args apply eggnogg_rl.launcher
monkeytype $args apply eggnogg_rl.benchmarks
