#!/usr/bin/env bash
if [ "$1" = "--fast" ]
then
	python -m unittest discover -s tests/fast
else
	python -m unittest discover -f tests/slow --verbose
fi
