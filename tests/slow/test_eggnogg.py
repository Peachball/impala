"""
Tests eggnogg environment and logistics

Very slow because of the need to spawn vnc servers
"""
import time
import unittest
import warnings
from pathlib import Path

import numpy as np

from eggnogg_rl import launcher, benchmarks


class TestEggnogg(unittest.TestCase):
    def test_get_frame(self):
        """
        checks getting frame works
        """
        game = launcher.DesktopEggnogg()
        self.assertTrue(isinstance(game.get_frame(), np.ndarray))
        game.close()

    def test_game_screen_recognition(self):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            scr = launcher.GameScreen
            base_dir = Path('data/')
            test_images = {
                'level_image.npy': scr.LEVEL_SELECT,
                'title_image.npy': scr.TITLE,
                'game_image_0.npy': scr.GAME,
                'game_image_1.npy': scr.GAME,
                'pause_image.npy': scr.PAUSE
            }
            for path, label in test_images.items():
                image = np.load(str(base_dir / path))
                self.assertEqual(launcher.get_game_mode(image), label)

    def test_benchmarks(self):
        """
        Make sure the benchmark code at least runs
        """
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            fps = benchmarks.calculate_fps()


class TestVNCEggnogg(unittest.TestCase):
    def test_vnc_get_frame(self):
        """
        makes sure vnc works
        """
        self.assertTrue(isinstance(self.game.get_frame(), np.ndarray))

    def test_restarting_game(self):
        def _check_mode(mode):
            self.assertEqual(self.game.current_mode(), mode)

        _check_mode(launcher.GameScreen.TITLE)
        self.game.reset_game()
        _check_mode(launcher.GameScreen.GAME)
        time.sleep(3.1)  # Required because of countdown
        self.game.reset_game()
        _check_mode(launcher.GameScreen.GAME)

    def setUp(self):
        self.game = launcher.VNCEggnogg()

    def tearDown(self):
        self.game.close()


class TestEggnoggEnv(unittest.TestCase):
    def test_sample_run(self):
        with launcher.temp_vnc_env() as env:
            obs = env.reset()
            for i in range(20):
                new_obs, *_ = env.step(env.action_space.sample())
                self.assertEqual(obs.shape, new_obs.shape)
