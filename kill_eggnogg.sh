#!/usr/bin/env bash
pgrep eggnoggplus | xargs -L 1 kill -9
pgrep eggnoggplus | xargs -L 1 ps -o ppid= -p | xargs -L 1 kill -9
