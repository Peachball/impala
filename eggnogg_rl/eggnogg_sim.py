import cv2

from eggnogg_rl import launcher


def main() -> None:
    print("Clearing servers")
    launcher.kill_all_vncservers()
    frames = 20
    with launcher.temp_vnc_game() as game:
        for i in range(frames):
            cv2.imshow('game', game.get_frame())
            cv2.waitKey(1)
        game.reset_game()
        while True:
            cv2.imshow('game', game.get_frame())
            key = cv2.waitKey(1)
            if key == ord('q'):
                break
            if key == ord('r'):
                game.reset_game()


def run_random_agent() -> None:
    try:
        env = launcher.VNCEggnoggEnv()
        obs = env.reset()
        for _ in range(1000):
            _, r, *_ = env.step(env.action_space.sample())
            env.render()
    finally:
        env.close()


if __name__ == '__main__':
    run_random_agent()
