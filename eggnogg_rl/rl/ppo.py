"""
Proximal policy optimization algorithm
"""
import re
from typing import Tuple, Union

import gym
import numpy as np
import torch


def infer_conv_output_shape(inp_shape: Tuple[int, int], kernel_size: int,
                            stride: int) -> Tuple[int, int]:
    new_dim = lambda i: int((i - (kernel_size - 1) - 1) / stride + 1)
    new_h = new_dim(inp_shape[0])
    new_w = new_dim(inp_shape[1])
    return (new_h, new_w)


def weight_initializer(module):
    if isinstance(module, torch.nn.Linear) or isinstance(
            module, torch.nn.Conv2d):
        torch.nn.init.xavier_normal_(module.weight)
        module.bias.data.fill_(0.0)
    elif isinstance(module, torch.nn.LSTM):
        for name, param in module.named_parameters():
            if re.search('^bias', name):
                param.data.fill_(0.0)
            if re.search('^bias_hh', name):
                param.data[module.hidden_size:module.hidden_size * 2] = 1
            if re.search('^weight_ih', name):
                torch.nn.init.normal_(
                    param,
                    std=(2 / (module.hidden_size + module.input_size))**.5)
            if re.search('^weight_hh', name):
                torch.nn.init.normal_(param, std=(1 / module.hidden_size))


class VisionEmbedding(torch.nn.Module):
    def __init__(self, inp_shape: Tuple[int, int, int], out_dim: int) -> None:
        """
        Assumes inp_shape comes in HWC format
        """
        super(VisionEmbedding, self).__init__()
        # Architecture taken from A3C paper
        # Link: https://arxiv.org/pdf/1602.01783.pdf
        conv_info = [(16, 8, 4), (32, 4, 2)]  # [(filters, size, stride)]
        in_channels = [inp_shape[-1]] + [v[0] for v in conv_info[:-1]]
        out_conv_shape = inp_shape[:2]
        for inf in conv_info:
            out_conv_shape = infer_conv_output_shape(out_conv_shape, inf[1],
                                                     inf[2])
        self.layers = torch.nn.ModuleList([
            torch.nn.Conv2d(
                in_c, out_c[0], kernel_size=out_c[1], stride=out_c[2])
            for out_c, in_c in zip(conv_info, in_channels)
        ])
        self.out_layer = torch.nn.Linear(
            conv_info[-1][0] * np.prod(out_conv_shape), out_dim)

    def forward(self, x):
        orig_shape = x.size()
        x = x.permute(0, 3, 1, 2)
        x = x.view(np.prod(x.size()[:-3]), *x.size()[-3:])
        for l in self.layers:
            x = l(x)
            x = torch.nn.functional.selu(x)
        x = x.view(*orig_shape[:-3], np.prod(x.size()[-3:]))
        return self.out_layer(x)


class LSTMPolicy(torch.nn.Module):
    def __init__(self, observation_space: gym.Space,
                 action_space: gym.Space) -> None:
        super(LSTMPolicy, self).__init__()
        assert isinstance(action_space, gym.spaces.MultiBinary)
        assert isinstance(observation_space, gym.spaces.Box)
        # Note one unit of the embedding is which player is playing
        self.embedding_dim = embedding_dim = 256
        self.action_space = action_space
        self.observation_space = observation_space

        self.hidden_size = 60
        self._init_hidden = torch.nn.Parameter(
            torch.zeros((1, self.hidden_size * 2)) + 1e-5)
        self.lstm = torch.nn.LSTM(embedding_dim, self.hidden_size)

        self.policy_head = torch.nn.Linear(self.hidden_size, action_space.n)
        self.value_head = torch.nn.Linear(self.hidden_size, 1)

        # For curiosity
        self.state_model = torch.nn.Linear(self.hidden_size + action_space.n,
                                           self.hidden_size)

        if len(observation_space.shape) == 1:
            self._embed = torch.nn.Sequential([
                torch.nn.Linear(observation_space.shape[0], embedding_dim - 1),
                torch.nn.LeakyReLU()
            ])
        elif len(observation_space.shape) == 3:
            self._embed = VisionEmbedding(observation_space.shape,
                                          embedding_dim - 1)
        else:
            raise NotImplementedError(
                "Unimplemented observation space {}".format(observation_space))

        # Initialize weights
        self.apply(weight_initializer)

    def is_cuda(self):
        return next(self.parameters()).is_cuda

    def embed(self, x: torch.Tensor, player: Union[None, int, torch.Tensor],
              state: torch.Tensor):
        embedding = self._embed(x)
        bs = x.size()[0]
        if isinstance(player, int):
            embedding = torch.cat(
                [embedding, torch.ones(bs, 1) * player], dim=1)
        if isinstance(player, torch.Tensor):
            embedding = torch.cat([embedding, player[:, None]], dim=1)
        elif player is None:
            player_values = torch.cat(
                [torch.zeros(bs, 1), torch.ones(bs, 1)], dim=0)
            if self.is_cuda():
                player_values = player_values.cuda()
            doubled_embedding = torch.cat([embedding, embedding], dim=0)
            embedding = torch.cat([doubled_embedding, player_values], dim=1)
        else:
            raise ValueError("Player value invalid: {}".format(player))
        recurrent_embedding, new_state = self.lstm(embedding[None, :, :],
                                                   state)
        return recurrent_embedding[0], new_state

    def init_hidden(self, bs: int):
        h = self._init_hidden[:, :self.hidden_size]
        c = self._init_hidden[:, self.hidden_size:]
        adjust_dim = lambda v: v[None, :, :].expand(-1, bs, -1).contiguous()
        return adjust_dim(h), adjust_dim(c)

    def _to_torch(self, x: np.ndarray) -> torch.Tensor:
        x = torch.from_numpy(x)
        if self.is_cuda():
            x = x.cuda()
        return x

    def forward(self, x: Union[torch.Tensor, np.ndarray], state: torch.Tensor,
                player: Union[int, None]):
        """
        Expects x to have shape (batch_size, *feature_dims)

        if player is None, get actions for both players (output batch_size
        doubles, with first half being for the left player)
        """
        if isinstance(x, np.ndarray):
            x = self._to_torch(x)
        embedding, new_state = self.embed(x, player, state)
        action_dist = self.policy_head(embedding)
        value_head = self.value_head(embedding)
        return action_dist, value_head, new_state, embedding

    def get_action(self, obs: np.ndarray,
                   state: Tuple[np.ndarray, np.ndarray]):
        with torch.no_grad():
            if state is None:
                state = self.init_hidden(2)
            act, _, hidden, _ = self(
                torch.from_numpy(obs[None, :, :, :].astype('float32') / 255),
                state, None)
            p_act = torch.sigmoid(act).numpy()
            act = np.random.rand(*p_act.shape) < p_act
            return ((act[0], act[1]), p_act), hidden

    def update_hidden(self, hidden: torch.Tensor,
                      mask: np.ndarray) -> torch.Tensor:
        mask = torch.from_numpy(mask[None, :, None].astype('float32'))
        if self.is_cuda():
            mask = mask.cuda()
        nh, nc = self.init_hidden(hidden[0].size()[1])

        def _upd(h, n):
            return n * mask + h * (1 - mask)

        return (_upd(hidden[0], nh), _upd(hidden[1], nc))

    def batch_predictions(self, obs, hidden, mask, player=None):
        pred_actions: List[torch.Tensor] = []
        pred_values: List[torch.Tensor] = []
        embeddings: List[torch.Tensor] = []
        for t in range(obs.shape[0]):
            hidden = self.update_hidden(hidden, mask[t])
            p = None
            if player is not None:
                p = player[t]
            act, val, hidden, e = self(obs[t], hidden, p)
            pred_actions.append(act)
            pred_values.append(val)
            embeddings.append(e)
        all_embeddings = torch.stack(embeddings)
        all_acts = torch.stack(pred_actions)
        all_values = torch.stack(pred_values)
        return all_embeddings, all_acts, all_values
