"""
    Collection of models compatible with various tasks
"""
import abc
from typing import Any

import numpy as np
import tensorflow as tf

_MODEL_ZOO = {}


def get_model(name: str):
    """
        Gets model with the corresponding name (e.g. LSTMModel)
    """
    return _MODEL_ZOO[name.lower()]


def zoo_model(name):
    def decorator(cls):
        _MODEL_ZOO[name] = cls
        return cls

    return decorator


class Model(abc.ABC):
    """
        Model for other scripts for use in training
    """

    def __init__(self, inp_dim, out_dim, name="model", reuse=None):
        self.inp = tf.placeholder(tf.float32, [None] + list(inp_dim))
        self.out_dim = out_dim
        with tf.variable_scope(name, reuse=reuse):
            self.value, self.action = self._build(self.inp)
            var_scope = tf.get_variable_scope().name
        self.variables = tf.get_collection(
            tf.GraphKeys.GLOBAL_VARIABLES, scope=var_scope)

    def set_params(self, params: Any, sess: tf.Session):
        """
            Set parameters of the model to params
        """
        [sess.run(v.assign(p)) for v, p in zip(self.variables, params)]

    def get_params(self, sess: tf.Session) -> Any:
        """
            Get current model params
        """
        return [sess.run(v) for v in self.variables]

    @abc.abstractmethod
    def act(self, obs: np.ndarray, sess: tf.Session) -> (float, int):
        """
            Returns probability and action after an observation
        """
        pass

    @abc.abstractmethod
    def _build(self, inp: tf.Tensor) -> tf.Tensor:
        """
            Constructs tensorflow model from input tensor
        """
        pass


@zoo_model('lstm')
class LSTM(Model):
    def _build(self, inp: tf.Tensor) -> tf.Tensor:
        # Assumes input shape is of the form:
        # (batch, time, input...)
        assert len(inp.get_shape().as_list()) == 5
        assert len(self.out_dim) == 1
        bs, ts = tf.shape(inp)[0], tf.shape(inp)[1]

        self.seq_lengths = tf.placeholder(tf.int32, [None])
        net = tf.reshape(inp, [-1] + inp.get_shape().as_list()[2:])
        for i in range(2):
            net = tf.layers.conv2d(
                net, 16 * 2 ** i, 8 // 2 ** i, strides=4 // 2 ** i)
            net = tf.nn.elu(net)

        net = tf.layers.flatten(net)
        net = tf.layers.dense(net, 256)
        net = tf.nn.elu(net)

        net = tf.reshape(net, [bs, ts, net.get_shape().as_list()[-1]])

        self.lstm = tf.contrib.rnn.LSTMCell(256)
        self.state = self.lstm.zero_state(bs, tf.float32)
        net, self.nstate = tf.nn.dynamic_rnn(
            self.lstm,
            net,
            sequence_length=self.seq_lengths,
            initial_state=self.state)
        net = net[:, -1, :]
        value = tf.layers.dense(net, 1)
        value = tf.reshape(value, [bs, ts])

        action = tf.layers.dense(net, self.out_dim[0])
        self.action_logits = action
        action = tf.nn.softmax(action)
        action = tf.reshape(action, [bs, ts, self.out_dim[0]])

        return value, action

    def act(self, obs: np.ndarray, sess: tf.Session) -> (float, int):
        probs, nstate = sess.run(
            [self.action, self.nstate],
            feed_dict={
                self.inp: obs[None, None],
                self.seq_lengths: [1]
            })
        probs = np.squeeze(probs)
        a = np.random.choice(probs.size, size=1, p=probs).squeeze()
        return probs[a], a, nstate
